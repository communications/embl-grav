[![pipeline status](https://git.embl.de/grp-stratcom/embl-grav/badges/g5_embl-ci/pipeline.svg)](https://git.embl.de/grp-stratcom/embl-grav/commits/g5_embl-ci)


# EMBL Grav configuration and Gantry theme

This repository contains the code needed to integrate Grav and Gantry with EMBL's look and feel.

## What's inside

There are four branches that contain:

- `master` Bonus branch that has readme, directions
- `grav_initial_configuration` The Grav (NOT Gantry) initial extra config (config, data, pages) folder
- `g5_embl` The whole default EMBL Gantry g5_embl theme
- `g5_embl-ci` This is the g5_embl theme minus CI bits, like: `g5_embl/common`. This is the only branch deployed on tagging with CI.

In theory, we don't need the `g5_embl-ci` branch and extra things could be removed by the shell script, but it's a little unclear what's "code" and what's "config", and we might find we need to add/remove things from the CI process -- and we don't want to have to tinker with the shell script every time.

## How to use

This repo serves two purposes: initial installation and maintenance.

### 1. Initial installation

1. Install Grav from [GetGrav.org](https://getgrav.org/)
2. Pull configuration from the branch `grav_initial_configuration`
3. Install required Grav plugins:
   `bin/gpm install grav5 problems error simplesearch taxonomylist`
4. Pull initial theme from the branch `g5_embl`

### 2. Maintenance

A CI process + shell script will pull from the branch `g5_embl-ci` and overwrite protions of the `g5_embl` theme.
